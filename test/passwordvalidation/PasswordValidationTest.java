/*
 * Taghreed Safaryan
 *  991494905
 */
package passwordvalidation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class PasswordValidationTest {
    
    public PasswordValidationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    //Length of password
    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validatePassword Length regular");
        String password = "a2e@fgh#iJ";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The length of the password does not meet the requirement", expResult, result);
    }
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword Length Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The length of the password does not meet the requirement", expResult, result);
    }
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword Length boundary in");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The length of the password does not meet the requirement", expResult, result);
    }
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword Length boundary out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The length of the password does not meet the requirement", expResult, result);
    }
    
    //Test special character
    @Test
    public void testValidatePasswordSpecialChars() {
        System.out.println("validatePassword Special chars regular");
        String password = "a2e@fgh#iJ";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The special characters of the password meet the specail chars requirement", expResult, result);
    }
    public void testValidatePasswordSpecialCharacterException() {
        System.out.println("validatePassword Special Character Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The special characters of the password does not meet the requirement", expResult, result);
    }
    public void testValidateSpecialCharacterBoundaryIn() {
        System.out.println("validatePassword Special Character boundary in");
        String password = "abcd%Tfgh2";
        boolean expResult = true;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The special characters of the password does not meet the requirement", expResult, result);
    }
    public void testValidateSpecialCharacterBoundaryOut() {
        System.out.println("validatePassword Special Character boundary out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The special characters of the password does not meet the requirement", expResult, result);
    }
    
    //Test Uppercase
    @Test
    public void testValidatePasswordUpperCase() {
        System.out.println("validatePassword UpperCase regular");
        String password = "a2e@fgh#iJ";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The UpperCase characters of the password meet the requirement", expResult, result);
    }
    public void testValidatePasswordUpperCaseException() {
        System.out.println("validatePassword UpperCase Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The UpperCase of the password does not meet the requirement", expResult, result);
    }
    public void testValidateSpecialUpperCaseBoundaryIn() {
        System.out.println("validatePassword UpperCase boundary in");
        String password = "abc@2Ffi%gh";
        boolean expResult = true;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The UpperCase characters of the password does not meet the requirement", expResult, result);
    }
    public void testValidateUpperCaseBoundaryOut() {
        System.out.println("validatePassword UpperCase boundary out");
        String password = "abcdefgfd";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The UpperCase characters of the password does not meet the requirement", expResult, result);
    }  
    
    //Test At least one Digit
    @Test
    public void testValidatePasswordDigit() {
        System.out.println("validatePassword Digit regular");
        String password = "123456789";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The Digit of the password meet the requirement", expResult, result);
    }
    public void testValidatePasswordDigitException() {
        System.out.println("validatePassword Digit Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The Digit of the password does not meet the requirement", expResult, result);
    }
    public void testValidateSpecialDigitBoundaryIn() {
        System.out.println("validatePassword Digit boundary in");
        String password = "abc@2Efgh";
        boolean expResult = true;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The Digit of the password does not meet the requirement", expResult, result);
    }
    public void testValidateDigitBoundaryOut() {
        System.out.println("validatePassword Digit boundary out");
        String password = "abc2efgfd";
        boolean expResult = false;
        boolean result = PasswordValidation.validatePassword(password);
        assertEquals("The Digit of the password does not meet the requirement", expResult, result);
    }  
    
}
