/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package passwordvalidation;

import java.util.regex.Pattern;

public class PasswordValidation {
    
    //Password Length Validator 
    private static boolean checkLength(String password){
    return password.length() >=8;
       }
    
    //Password special character validator 
    private static boolean checkSpecialChars(String password){
        String regex = "(.*\\W.*)";  //(?=.*[@#$%^&+=])
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(password).matches(); 
       }
    
    //Password Upper case validator 
    private static boolean checkUpperCaseChars(String password){
        String regex = "(?=.*[A-Z])"; //uppercase 
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(password).matches(); 
       }
    
    //Password At least one digit validator
    private static boolean checkDigits(String password){
        String regex = "(?=.*\\d)";  // at least one digit
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(password).matches();  
       }
    
    public static boolean validatePassword (String password ){
    return checkLength(password) && checkSpecialChars(password)&& checkUpperCaseChars(password) && checkDigits(password);
    }
}
    
